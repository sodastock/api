#!/bin/bash

mkdir ../public/docs

php ../vendor/bin/openapi --bootstrap ./swagger-constants.php --output ../public/docs ./swagger-v1.php ../app/Http/Controllers/API/V1
