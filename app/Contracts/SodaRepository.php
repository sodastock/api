<?php

namespace SodaStock\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SodaRepository.
 *
 * @package namespace SodaStock\Contracts;
 */
interface SodaRepository extends RepositoryInterface
{
    public function deleteMultiple(array $ids);
}
