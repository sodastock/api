<?php

namespace SodaStock\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\SodaStock\Contracts\SodaRepository::class, \SodaStock\Repositories\SodaRepositoryEloquent::class);
        //:end-bindings:
    }
}
