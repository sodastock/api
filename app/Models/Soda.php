<?php

namespace SodaStock\Models;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Soda.
 *
 * @package namespace SodaStock\Models;
 */
class Soda extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand',
        'volume',
        'type',
        'quantity',
        'cost',
    ];
}
