<?php

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Generate pagination of array or collection.
 *
 * @param array|Collection $items
 * @param int              $perPage
 * @param int              $page
 * @param array            $options
 *
 * @return LengthAwarePaginator
 */
if (!function_exists('paginateCollection')) {
    function paginateCollection($items, $perPage = 15, $page = null, $pageName = 'page')
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, [
            'path'     => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }
}

/**
 * Extract ids or id
 *
 * @param string $ids
 *
 * @return string|array
 */
if (!function_exists('getIds')) {
    function getIds($ids)
    {
        $idsArr = explode(',', $ids);

        if (count($idsArr) > 1)
            return $idsArr;

        return $idsArr[0];
    }
}
