<?php

namespace SodaStock\Rules;

use Illuminate\Contracts\Validation\Rule;
use SodaStock\Models\Soda;

class UniqueSodaRule implements Rule
{

    private $volume;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $volume)
    {
        $this->volume = $volume;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (request()->method() === 'PATCH') {
            $exists = Soda::where('brand', $value)->where('volume', $this->volume)->first();

            if (is_null($exists))
                return true;

            if ($exists->_id === request()->soda)
                return true;

            return false;
        }

        $exists = Soda::where('brand', $value)->where('volume', $this->volume)->first();

        return is_null($exists);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Não pode haver cadastro de refrigerantes da mesma marca com a mesma litragem.';
    }
}
