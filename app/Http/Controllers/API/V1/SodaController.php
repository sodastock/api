<?php

namespace SodaStock\Http\Controllers\API\V1;

use SodaStock\Contracts\SodaRepository;
use SodaStock\Http\Resources\SodaResource;
use SodaStock\Http\Requests\SodaFormRequest;
use SodaStock\Http\Controllers\BaseController;

class SodaController extends BaseController
{
    /**
     * @OA\Info(title="SodaStock API", version="1.0")
     * @OA\SecurityScheme(
     *      securityScheme="bearerAuth",
     *      in="header",
     *      name="bearerAuth",
     *      type="http",
     *      scheme="bearer",
     *      bearerFormat="JWT",
     * ),
     */

    /**
     * SodaController constructor.
     *
     * @param SodaRepository $repository
     */
    public function __construct(SodaRepository $repository)
    {
        $this->repository = $repository;
        $this->resource = new SodaResource($repository);

        return parent::__construct();
    }

    /**
     * @OA\Get(
     *     tags={"Soda"},
     *     summary="Returns a list of sodas",
     *     description="Returns a object of sodas",
     *     path="/api/soda",
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="search",
     *         in="query",
     *         description="Search term",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="searchFields",
     *         in="query",
     *         description="Search fields",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page of pagination",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(response="200", description="A list with sodas"),
     * ),
     *
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * @OA\Get(
     *     tags={"Soda"},
     *     summary="Returns a soda by Id",
     *     description="Returns a object of soda by Id",
     *     path="/api/soda/{id}",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(response="200", description="Show soda"),
     * ),
     *
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * @OA\Post(
     *     tags={"Soda"},
     *     summary="Store a soda",
     *     description="Store a soda",
     *     path="/api/soda",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(response="200", description="Store soda"),
     *     @OA\Response(response=401, description="Unauthorized"),
     * ),
     *
     */
    public function store(SodaFormRequest $request)
    {
        return parent::_store($request);
    }

    /**
     * @OA\Patch(
     *     tags={"Soda"},
     *     summary="Update a soda",
     *     description="Update a soda",
     *     path="/api/soda/{id}",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(response="200", description="Update soda"),
     * ),
     *
     */
    public function update(SodaFormRequest $request, $id)
    {
        return parent::_update($request, $id);
    }

    /**
     * @OA\Delete(
     *     tags={"Soda"},
     *     summary="Delete a soda",
     *     description="Delete a soda",
     *     path="/api/soda/{id}",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(response="200", description="Destroy soda"),
     * ),
     *
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
