<?php

namespace SodaStock\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * @var Repository
     */
    protected $repository;
    /**
     * @var Resource
     */
    protected $resource;
    /**
     * @var string
     */
    protected $resourceClass;
    /**
     * @var int
     */
    protected $perPage = 10;
    /**
     * @var array
     */
    protected $data = [];

    public function __construct()
    {
        $this->resourceClass = get_class($this->resource);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = $this->resource->collection($this->repository->all());

        $items = paginateCollection($collection, $this->perPage);

        return $items;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = new $this->resourceClass($this->repository->find($id));

        return response()->json([
            'data' => $item,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function _store(Request $request)
    {
        $item = new $this->resourceClass($this->repository->create($request->all()));

        return response()->json([
            'message' => 'Item created.',
            'data'    => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function _update(Request $request, $id)
    {
        $item = new $this->resourceClass($this->repository->update($request->all(), $id));

        return response()->json([
            'message' => 'Item updated.',
            'data'    => $item,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = getIds($id);

        if (!is_array($items)) {
            $deleted = $this->repository->delete($id);
        } else {
            $deleted = $this->repository->deleteMultiple($items);
        }

        return response()->json([
            'message' => "{$deleted} item(s) deleted.",
        ]);
    }
}
