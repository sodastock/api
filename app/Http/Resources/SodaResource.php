<?php

namespace SodaStock\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SodaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'brand'    => $this->brand,
            'volume'   => $this->volume,
            'type'     => $this->type,
            'quantity' => $this->quantity,
            'cost'     => $this->cost
        ];
    }
}
