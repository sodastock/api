<?php

namespace SodaStock\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use SodaStock\Rules\UniqueSodaRule;

class SodaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // unique:sodas,brand,' . $this->brand . ',id,volume,' . $this->volume (works ocasionally)
        return [
            'brand'    => ['required', 'max:30', new UniqueSodaRule($this->volume)],
            'volume'   => 'required|integer',
            'type'     => 'required|in:pet,garrafa,lata',
            'quantity' => 'required|integer',
            'cost'     => 'required|numeric',
        ];
    }

    /**
     * Set custom messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'brand.unique' => 'Não pode haver cadastro de refrigerantes da mesma marca com a mesma litragem.'
        ];
    }

    /**
     * Set the attributes pt-br lang.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'brand'    => 'marca',
            'volume'   => 'litragem',
            'type'     => 'tipo',
            'quantity' => 'quantidade',
            'cost'     => 'valor (R$)',
        ];
    }
}
