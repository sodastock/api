<?php

namespace SodaStock\Repositories;

use Prettus\Repository\Contracts\CacheableInterface;
use SodaStock\Models\Soda;
use SodaStock\Support\RequestCriteria;
use SodaStock\Contracts\SodaRepository;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class SodaRepositoryEloquent.
 *
 * @package namespace SodaStock\Repositories;
 */
class SodaRepositoryEloquent extends BaseRepository implements SodaRepository, CacheableInterface
{

    use CacheableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'brand',
        'volume',
        'quantity',
        'cost'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Soda::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Remove item (multiple)
     */
    public function deleteMultiple(array $ids)
    {
        return $this->model->destroy($ids);
    }
}
