<table align="center"><tr><td align="center" width="9999">
<img src="documentation/images/soda.jpg" align="center" width="150" alt="SodaStock">

# SodaStock

SodaStock is a stock control system for the management of refrigerants. Developed for Fortics FullStack Developer position.

</td></tr></table>

## Table of Contents

-   [Stack](#stack)

-   [Tools](#tools)

-   [Documentation](#documentation)

-   [Tests](#tests)

### Run

`cp .env.example .env`

`docker-compose up -d`

`docker-compose exec app composer install`

`docker-compose exec app php artisan key:generate`

`docker-compose exec app php artisan migrate`

### Stack

-   Laravel 5.8
-   MongoDB
-   Redis
-   Nginx
-   Docker

### Tools

-   Swagger
-   CORS
-   JWT
-   Resource
-   Helpers
-   Rules
-   Repository
-   Cache (redis)

### Documentation

(Dev)

![Swagger](documentation/images/swagger.jpg "Swagger")

### Tests

![Test](documentation/images/test.jpg "Test")
