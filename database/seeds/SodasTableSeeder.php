<?php

use Illuminate\Database\Seeder;
use SodaStock\Models\Soda;

class SodasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Soda::create([
            'brand' => 'CocaCola',
            'volume' => 1000,
            'type' => 'garrafa',
            'quantity' => 100,
            'cost' => 5.0,
        ]);
    }
}
