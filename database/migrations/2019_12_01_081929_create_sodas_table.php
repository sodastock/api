<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSodasTable.
 */
class CreateSodasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sodas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand', 30);
            $table->integer('volume');
            $table->enum('type', ['pet', 'lata', 'garrafa']);
            $table->float('cost');
            $table->integer('quantity');
            $table->timestamps();

            $table->unique(['brand', 'volume']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sodas');
    }
}
